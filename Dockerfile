FROM debian:buster-slim

RUN apt update
RUN apt upgrade -y

#install dependencies
RUN apt install -y curl openssl libssl-dev pkg-config rustc cargo git mercurial capnproto clang sqlite3 libsqlite3-0 libsqlite3-dev python3 python3-lxml build-essential automake libtool autoconf make nettle-dev capnproto uuid-dev python3-setuptools libboost-python-dev libboost-locale1.67-dev nano


ENV HOME=/root

RUN echo $PATH

#create folders
RUN mkdir $HOME/code
RUN mkdir $HOME/code/common
RUN mkdir $HOME/include
RUN mkdir $HOME/lib
RUN mkdir $HOME/bin
RUN mkdir $HOME/share

#build and install sequoia
RUN mkdir -p $HOME/code/common/sequoia
WORKDIR $HOME/code/common/sequoia
RUN git clone https://gitlab.com/sequoia-pgp/sequoia.git .
RUN git checkout pep-engine
RUN make build-release PYTHON=disable
RUN make install PYTHON=disable PREFIX="$HOME"

#build and install YML2
RUN apt install -y  python-setuptools
RUN mkdir -p $HOME/code/common/yml2
ENV PATH="$HOME/code/common/yml2:${PATH}"
WORKDIR $HOME/code/common/yml2
RUN hg clone https://pep.foundation/dev/repos/yml2 .
RUN make

#build and install libetpan
RUN mkdir -p $HOME/code/common/libetpan
WORKDIR $HOME/code/common/libetpan
RUN git clone https://github.com/fdik/libetpan .
RUN mkdir $HOME/code/common/libetpan/build
RUN ./autogen.sh --prefix=$HOME
RUN make install

#build and install asn1c
RUN mkdir -p $HOME/code/common/asn1c
WORKDIR $HOME/code/common/asn1c
RUN git clone git://github.com/vlm/asn1c.git .
RUN git checkout tags/v0.9.28 -b pep-engine
RUN autoreconf -iv
RUN ./configure --prefix="$HOME"
RUN make install

#build pEp Engine
RUN mkdir -p $HOME/code/pEpEngine
WORKDIR $HOME/code/pEpEngine
RUN hg clone https://pep.foundation/dev/repos/pEpEngine/ .
COPY local.conf $HOME/code/pEpEngine

RUN make 
RUN make install
RUN make db dbinstall

#build pEp Adapter
RUN mkdir -p $HOME/code/common/libpEpAdapter
WORKDIR $HOME/code/common/libpEpAdapter
RUN hg clone https://pep.foundation/dev/repos/libpEpAdapter/ .
RUN make
RUN make install PREFIX="$HOME"

#build Python Adapter
RUN mkdir -p $HOME/code/pEpPythonAdapter
WORKDIR $HOME/code/pEpPythonAdapter
RUN hg clone https://pep.foundation/dev/repos/pEpPythonAdapter/ .
RUN make

#set environment variables for libraries and python
RUN echo 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/lib' >> $HOME/.bashrc
RUN echo 'export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:$HOME/lib' >> $HOME/.bashrc
RUN echo 'export PYTHONPATH=$PYTHONPATH:$HOME/code/pEpPythonAdapter/build/lib.linux-x86_64-3.7' >> $HOME/.bashrc
RUN echo 'export PYTHONPATH=$PYTHONPATH:$HOME/code/pEpPythonAdapter/build/lib.macosx-10.9-x86_64-3.8' >> $HOME/.bashrc
RUN echo 'export PYTHONPATH=$PYTHONPATH:$HOME/code/pEpPythonAdapter/src' >> $HOME/.bashrc

WORKDIR /

ENTRYPOINT /bin/bash



